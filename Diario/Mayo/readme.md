## MAYO

|Semana|Descripción|
|-----|--------|
|6-10|Durante el último mes en la empresa, he comenzado a realizar una nueva tarea: ayudar a Álvaro a incluir en el CRM a todos aquellos usuarios que han completado los "trainings" mencionados en mi diario. Este trabajo me tomará bastante tiempo, ya que implica sincronizar varios campos, como el del training, con la cuenta de una persona que está relacionada con una empresa. La complejidad aumenta cuando algunos usuarios están duplicados, por lo que, tras consultar con IT, me pidieron que identificara y combinara los usuarios duplicados.|
|13-17| Esta semana he continuado con los trainings y escalando los casos para mis compañeros, intentando cerrar la mayoría de ellos. Me pidieron que en toda mi estancia complete un total de 200 casos, en esta semana también fui con Circontrol a la Cecot, en el que explicamos nuestras vivencias a las personas que haran DUAL en el Nicolau Còpernic.|
|20-22| Semana muy triste para mi, ya que es la última, tiempo atrás me preguntaron si queria que continuar en la empresa y dije que estaria muy interesado, pero esta semana después hablar con el jefe, me ha dicho de que no se pueden permitir incorporar a un nuevo tecnico, y que aparte quieren a una persona que le apasione solucionar casos y que mejor que yo me dedique a lo que me apasioona, estos dias fueron de despedida con todas aquellas personas que había conocido, me sorprendio la cantidad de cosas que hice en 9 meses. Estoy muy agradecido de la oportunidad que se me ha dado.|


<td align="center"><a href="/Diario/readme.md">VOLVER</a></td>

