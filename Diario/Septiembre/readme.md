## SEPTIEMBRE

| Semana | Descripción |
| ------ | ----------- |
| 12-15 | Después de mis días festivos, regreso al trabajo, pero esta vez con una jornada de 4 horas, ya que ahora debo asistir a clases por las tardes. Durante esta semana, me pongo al día con la resolución de los casos pendientes y dedico tiempo a revisar, ordenar y limpiar mi correo electrónico. También me informan de que debo realizar un curso de electricidad, el cual llevaré a cabo durante esta semana y la próxima. |
| 18-22 | Durante esta semana, realizo y apruebo el curso de electricidad, donde se enseñan conceptos básicos sobre el funcionamiento de la electricidad, cómo medir distintos aspectos, y se destacan las cinco reglas de oro, entre otros temas. Gracias a este curso, adquiero los conocimientos necesarios para acceder al box, donde se realizan pruebas a los equipos. |
| 25-29 | Durante esta semana, continúo adquiriendo conocimientos sobre electricidad y su aplicación en el ámbito laboral. También participo en sesiones prácticas donde aplico lo aprendido en el curso. Además, aprovecho para repasar los casos pendientes y colaborar con mis compañeros en la resolución de problemas.|

<td align="center"><a href="/Diario/readme.md">VOLVER</a></td>