[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Fira+Code&pause=1000&color=4A5DF7&background=FFFFFF00&center=true&vCenter=true&random=true&width=435&lines=Welcome+to+my+dual+diary+%F0%9F%91%8B;I'm+Juan+Flores!)](https://git.io/typing-svg)


## WHO I AM?

Soy Juan Francisco Flores Fernández, estudiante del Instituto Nicolau Copèrnic, ubicado en Terrassa, Barcelona.

El Instituto Nicolau Copèrnic se encuentra en la ciudad de Terrassa, en la provincia de Barcelona. Este instituto ofrece una variedad de ciclos formativos, tanto de grado medio como de grado superior, abarcando diversas especialidades. En el centro, se imparten ciclos formativos de diferentes familias profesionales, con una matrícula que asciende a un considerable número de alumnos, lo que refleja su relevancia y reconocimiento en la comunidad educativa.

<div>
<p style = 'text-align:center;'>
<img src="https://triafutur.terrassa.cat/wp-content/uploads/2021/03/Logo-INS-Nicolau-Copernic.jpg" alt="Logo" width="400px">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/IESNicolauCopernic.jpg/520px-IESNicolauCopernic.jpg" alt="Centro" width="350px">
</p>
</div>


En el siguiente enlace, se pueden consultar las referencias normativas que rigen este ciclo formativo: [Referencias normativas.](https://xtec.gencat.cat/ca/curriculum/professionals/fp/titolsloe/infcomunicacions/)


#### Motivación

Mi motivación para elegir el ciclo de Desarrollo de Aplicaciones Multiplataforma (DAM) proviene de mi pasión por la tecnología. Desde pequeño, siempre me ha interesado cómo funcionan las aplicaciones, y esto me llevó a querer profundizar en este campo para adquirir conocimientos más avanzados y prácticos. Esta curiosidad inicial no solo me impulsó a explorar más sobre el desarrollo de software, sino que también me mostró la importancia de la aplicación práctica de los conocimientos teóricos.

Por esta razón, elegí la modalidad Dual de estancia en la empresa. Creo firmemente que el aprendizaje práctico y la experiencia laboral son esenciales para una formación completa. La modalidad Dual permite combinar el aprendizaje teórico en el instituto con la experiencia práctica en una empresa real, lo que me proporciona una visión más amplia y realista del mundo laboral. De esta manera, no solo aprendo los conceptos en un entorno académico, sino que también tengo la oportunidad de aplicar estos conocimientos en situaciones reales, enfrentándome a los desafíos que surgen en el día a día en una empresa.

Además, esta decisión también está influenciada por mis experiencias pasadas. No todas ellas han sido positivas, y por eso, quiero vivir una experiencia formativa y satisfactoria. Esta combinación de teoría y práctica no solo me ayudará a superar esas experiencias anteriores, sino que también me permitirá desarrollar habilidades y competencias cruciales para mi futuro profesional en el campo de la tecnología y la programación.

Con la estancia en la empresa, espero alcanzar los siguientes objetivos:

- Adquirir Experiencia Práctica: Desarrollar y aplicar los conocimientos teóricos adquiridos durante el ciclo en proyectos reales, mejorando así mis habilidades técnicas y prácticas.
- Mejorar Mis Competencias: Aprender de profesionales, comprender mejor los procesos y las dinámicas de trabajo en un entorno laboral real y mejorar mis competencias en trabajo en equipo y gestión de proyectos.
- Prepararme para el Futuro Profesional: Crear una red de contactos profesionales, obtener referencias y estar mejor preparado para entrar en el mercado laboral una vez finalizado el ciclo formativo.
- Conocer las Últimas Tecnologías: Tener acceso a las tecnologías y herramientas más actuales utilizadas en la industria, manteniéndome al día con las tendencias y novedades del sector.

En resumen, mi motivación y mis objetivos con el ciclo DAM y la modalidad Dual son adquirir una formación completa y práctica que me prepare para tener éxito en el mundo de la tecnología y la programación.


## EMPRESA DUAL
Actualmente, me encuentro realizando mi estancia Dual en Circontrol, una empresa dedicada al diseño, fabricación y comercialización de soluciones de carga para vehículos eléctricos, ubicada en Viladecavalls, Barcelona. Desde mi llegada, he sido asignado al Departamento de SAT (Servicio de Asistencia Técnica) como técnico internacional de posventa.

<div>
<p style = 'text-align:center;'>
<img src="https://circontrol.com/wp-content/uploads/2023/10/reunion-charin-avere-aedive-scaled-1.jpg" alt="Empresa" width="400px">
<img src="https://pbs.twimg.com/profile_images/1618894726416982018/xmbh-4Ds_400x400.jpg" alt="empresa" width="300px">

</p>
</div>

En Circontrol, el Departamento de SAT despliega un papel crucial, brindando soporte técnico y asistencia postventa a los clientes de la empresa. Esto implica resolver incidencias técnicas, realizar mantenimiento preventivo y correctivo, ofrecer formación tanto a clientes como a técnicos, y gestionar garantías y repuestos.

La empresa cuenta con una estructura organizativa bien definida, que incluye departamentos como I+D, producción, marketing y ventas, recursos humanos, entre otros. Sin embargo, mi labor se centra en el Departamento de SAT, donde trabajo codo a codo con profesionales en la resolución de problemas técnicos y en el mantenimiento de los productos de Circontrol. Por privacidad de datos no puedo dar más detalles de cómo está ubicada la empresa.

Dentro del departamento, tengo a mi disposición una variedad de recursos y herramientas. Desde ordenadores equipados con software especializado hasta dispositivos móviles y herramientas de medición, todo diseñado para facilitar mi trabajo y garantizar un alto nivel de eficiencia. También tenemos a disposición un lugar de pruebas llamado "boxes," en los que se hacen algunos tests sobre cargadores, contando con todo lo indispensable para lograr la seguridad del técnico.

## INFORMACIÓN DE MI ESTADA

#### Tipo de Contratación: Becario

Mi contrato como becario en Circontrol consiste en colaborar con el Departamento de SAT como técnico internacional de posventa. Mis responsabilidades incluyen brindar soporte técnico, resolver incidencias postventa, y contribuir al mantenimiento de los productos de la empresa.

#### Beneficios Sociales Disfrutados:

- Plaza de parking.
- Disponibilidad horaria flexible.
- Espacio para comer con compañeros de trabajo.

#### Horario realizado:
- 8 horas diarias desde el 3 de julio de 2023 hasta el 31 de julio de 2023.
- 4 horas diarias desde el 12 de septiembre de 2023 hasta el 15 de marzo de 2024.
- 6 horas diarias desde el 18 de marzo de 2024 hasta el 22 de mayo de 2024.

#### Temporización de la Estadía:
Comencé el 3 de julio de 2023 y finalicé el 22 de mayo de 2024.

#### Días de Ausencia por Vacaciones o Enfermedad: 
Tomé un período de vacaciones entre julio y septiembre, tuve algunas ausencias adicionales por enfermades o estuduios, ya que es algo de que en la empresa siempre me han facilitado.

## RELACIONES PROFESIONALES:

En mi estancia en estas practicas he dispuesto de dos tutores tanto en la empresa como en el insituto.

- En el instituto, mi primer tutor fue Guillermo Guerrero, seguido de Víctor Carretero. Ambos estuvieron siempre disponibles para responder mis preguntas y brindarme orientación en cada paso del camino.
- En la empresa, mi tutor fue Daniel Agulló, un experimentado Project Manager; siempre estaba dispuesto a ayudarme y aclarar cualquier duda que tuviera sobre mis responsabilidades y el funcionamiento de la empresa, incluso en ayudarme a que tipo de persona quiero llegar a ser.

En cuanto a la comunicación con mis compañeros de trabajo y los usuarios, fue una de las mejores experiencias que me llevé de esta oportunidad. Desde el primer día, me sentí parte de un equipo unido y colaborativo. Mis compañeros estaban siempre dispuestos a ayudarme y compartir su conocimiento, creando un ambiente de compañerismo y apoyo mutuo. Esta dinámica positiva se extendió también a la interacción con los clientes, donde siempre se priorizó la atención y el servicio de calidad.

En lo que respecta al seguimiento de la doble tutoría, puedo decir que en líneas generales fue bueno. Tanto los tutores del instituto como los de la empresa se coordinaron eficientemente para asegurarse de que mi experiencia formativa fuera lo mejor posible, siempre he estado informando de cualquier cambio.


## DIARIO DE PRÁCTICAS 
<div style="text-align: center;">
  <table style="margin: auto;">
    <tr>
      <td align="center"><a href="/Diario/Julio/readme.md">JULIO</a></td>
      <td align="center"><a href="/Diario/Septiembre/readme.md">SEPTIEMBRE</a></td>
      <td align="center"><a href="/Diario/Octubre/readme.md">OCTUBRE</a></td>
      <td align="center"><a href="/Diario/Noviembre/readme.md">NOVIEMBRE</a></td>
      <td align="center"><a href="/Diario/Diciembre/readme.md">DICIEMBRE</a></td>
      <td align="center"><a href="/Diario/Enero/readme.md">ENERO</a></td>
      <td align="center"><a href="/Diario/Febrero/readme.md">FEBRERO</a></td>
      <td align="center"><a href="/Diario/Marzo/readme.md">MARZO</a></td>
      <td align="center"><a href="/Diario/Abril/readme.md">ABRIL</a></td>
      <td align="center"><a href="/Diario/Mayo/readme.md">MAYO</a></td>
    </tr>
  </table>
</div>

#### Conocimientos
Después de explicar detalladamente mis actividades semanales, puedo recalcar lo siguiente:

Mi función principal era reparar cargadores. Mi día a día incluía atender a clientes, resolver problemas técnicos y llevar a cabo tareas administrativas. Además, aprendí sobre el funcionamiento interno de la empresa y cómo interactuar con los clientes.

Como algunos profesores ya sabrán, o como podéis observar por mis experiencias, no he podido poner en práctica mis estudios directamente en este trabajo. Sin embargo, fue en el trabajo donde realmente pude aplicar esos conocimientos en situaciones prácticas. Aprendí a adaptarme a problemas reales, a trabajar bajo presión y a comunicarme eficazmente con los clientes.

#### Habilidades Adquiridas
- Diagnóstico y Solución de Problemas Técnicos: Aprendí a diagnosticar y solucionar problemas específicos de los cargadores. Este proceso incluía identificar fallos, desmontar componentes y reparar o reemplazar las partes defectuosas.
- Manejo de Herramientas y Procedimientos de Reparación: Aprendí a utilizar diversas herramientas y a seguir procedimientos específicos para la reparación de los cargadores. Esto me permitió mejorar mis habilidades manuales y técnicas.
- Gestión del Tiempo y Organización: Los conceptos de gestión del tiempo y organización que fui aprendiendo poco a poco me ayudaron a mantenerme eficiente en mi trabajo diario. Pude aplicar estas habilidades para planificar mis tareas, priorizar las más urgentes y asegurarme de cumplir con los plazos establecidos.
- Comunicación Eficaz: Aprendí a comunicarme de manera efectiva con los clientes, especialmente en situaciones de tensión o conflicto. Esta habilidad es fundamental para mantener la satisfacción del cliente y asegurar que se sientan escuchados y atendidos.
- Adaptación y Trabajo bajo Presión: Adaptarme a problemas reales y trabajar bajo presión fueron habilidades que desarrollé en este trabajo. Aprendí a mantener la calma y a pensar de manera crítica para encontrar soluciones rápidas y efectivas.
- Conocimientos Administrativos: Realicé diversas tareas administrativas, lo que me permitió entender mejor el funcionamiento interno de la empresa y la importancia de mantener un registro preciso de todas las actividades.

A pesar de no haber podido aplicar directamente mis estudios de programación en el SAT, la experiencia me permitió adquirir habilidades y conocimientos valiosos. Estas habilidades, aunque no técnicas, son fundamentales para el desarrollo profesional y personal. La motivación de resolver problemas y la colaboración con mis compañeros hicieron que mi estancia fuera mucho más amena y enriquecedora. Estoy agradecido por esta oportunidad, ya que me ha permitido crecer como profesional y como persona, y forjar relaciones valiosas que llevaré conmigo siempre.

#### Sbid relación con horas introducidas

Durante mis prácticas, he incorporado los siguientes puntos en la plataforma sBid:

1.1. Trabajo sobre diferentes sistemas informáticos
- Descripción: Trabajar con distintos sistemas informáticos, identificando el hardware, sistemas operativos y aplicaciones instaladas, así como las restricciones o condiciones específicas de uso.
- Aplicación: Siempre he incorporado este punto cuando realizaba asistencias técnicas. Trabajaba bajo un sistema informático específico, como un cargador, lo cual me permitía identificar y gestionar el hardware y software asociados.

5.1. Reconocimiento de la funcionalidad de los sistemas ERP-CRM
- Descripción: Reconocer la funcionalidad de los sistemas ERP-CRM en un supuesto empresarial real, evaluando la utilidad de cada uno de sus módulos.
- Aplicación: Este punto lo utilizaba cuando interactuaba con el CRM, evaluando sus distintos módulos y funcionalidades en un contexto real de negocio.

5.4. Gestión de la información en sistemas ERP-CRM
- Descripción: Intervenir en la gestión de la información almacenada en sistemas ERP-CRM, garantizando su integridad.
- Aplicación: Utilizaba este punto cuando gestionaba la información dentro del CRM, ya sea creando, borrando o introduciendo datos, asegurándome de que toda la información se mantenía íntegra y correcta.

5.5. Desarrollo de componentes personalizados para ERP-CRM
- Descripción: Colaborar en el desarrollo de componentes personalizados para un sistema ERP-CRM, utilizando el lenguaje de programación proporcionado por el sistema.
- Aplicación: Dependiendo de la situación, aplicaba este punto cuando desarrollaba o personalizaba componentes dentro del CRM, ajustándolos a las necesidades específicas de la entidad y utilizando el lenguaje de programación adecuado.


#### Impacto
El trabajo en SAT tiene un impacto significativo en la empresa, especialmente en términos de satisfacción del cliente. Mantener a los clientes contentos es una de las prioridades más altas para la empresa, ya que la satisfacción del cliente está directamente relacionada con la retención y la adquisición de nuevos clientes. Cuantos más clientes satisfechos tengamos, mayor será el crecimiento y los ingresos de la empresa.

Si mi desempeño en el SAT no fuera el adecuado y los clientes no quedaran satisfechos, esto podría generar un alto grado de estrés, tanto para mí como para el equipo. La insatisfacción del cliente puede llevar a una pérdida de confianza en nuestros servicios y, en última instancia, a una disminución en la base de clientes de la empresa. Por lo tanto, cada interacción con un cliente es crucial y puede tener un impacto significativo en la reputación y el éxito financiero de la empresa.
A través de mi trabajo, he contribuido a garantizar que los clientes reciban el soporte necesario para sus necesidades, mejorando así su experiencia general con nuestros productos y servicios.

En resumen, el impacto de mi trabajo en la empresa es notable. Al asegurarme de que los clientes estén satisfechos, no solo he contribuido al éxito financiero de la empresa, sino que también he ayudado a mantener un ambiente de trabajo positivo y menos estresante para mí y mis compañeros. La importancia de este rol no puede subestimarse, ya que la satisfacción del cliente es clave para el crecimiento y la sostenibilidad de la empresa.


## VALORACIÓN PERSONAL

Este último año ha sido una experiencia increibe para mí, aunque no ha estado exenta de desafíos. Al principio, me sentí desmotivado porque no podía poner en práctica los conocimientos adquiridos durante mis estudios. Me preguntaba qué papel podría tener un programador en el Servicio de Asistencia Técnica (SAT). Sin embargo, poco a poco, la motivación de resolver problemas y enfrentar desafíos técnicos hizo que mi estancia fuera mucho más amena y gratificante.

He adquirido una cantidad considerable de conocimientos y he madurado tanto profesional como personalmente. A lo largo de este tiempo, descubrí habilidades que ni siquiera sabía que tenía, y el trabajo diario me permitió desarrollarlas y perfeccionarlas, como por ejemplo era enseñar a personas.

Un aspecto fundamental que quiero destacar es la importancia de los miembros de mi departamento. Sin ellos, mi experiencia no habría sido la misma. Siempre estuvieron dispuestos a ayudarme cuando tenía algún problema en el trabajo, ofreciendo su apoyo incondicional. Esta colaboración hicieron que cada día en el trabajo fuera más llevadero y, en muchos casos, agradable.

Además, nuestras reuniones informales los viernes, donde salíamos a tomar algo, o ir a jugar algunos partidos a futbol, fortalecieron nuestros lazos más allá del ámbito laboral. Gracias a esto, puedo decir con orgullo que no solo me voy ganando compañeros de trabajo, sino una verdadera familia.

En conclusión, a pesar de las dificultades iniciales, este año ha sido una etapa de gran aprendizaje y crecimiento para mí. He desarrollado nuevas competencias, mejorado mis habilidades técnicas y personales, y he forjado relaciones valiosas que llevaré conmigo siempre. Estoy agradecido por esta oportunidad y por el apoyo inestimable de mis compañeros de departamento, quienes hicieron que cada día de prácticas fuera una experiencia única y memorable.

<div>
<p style = 'text-align:center;'>
<img src="https://i.gyazo.com/thumb/3024/7dc98636c8ab3455fb7ab110d5d16a50-heic.jpg" alt="Grupal" width="300px">
</p>
</div>
